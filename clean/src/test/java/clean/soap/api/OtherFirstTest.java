package clean.soap.api;

import org.junit.Test;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class OtherFirstTest {

    /**
     * http://stackoverflow.com/questions/9922655/how-to-do-a-call-through-a-javax-xml-ws-service
     */

    @Test
    public void getTheWSDLTestA(){
        String wsdlURL = "https://soadev.pamperedchef.com/ConsultantReqService/V2/ConsultantCDHReqPS?wsdl";
        String namespace = "http://www.pamperedchef.com/ConsultantSvc/V2";
        String serviceName = "ConsultantSvc";
        QName serviceQN = new QName(namespace, serviceName);
        try {
            Service.create(new URL(wsdlURL), serviceQN);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getTheWSDLTestB(){
        String wsdlURL = "https://fmwcrp1.pamperedchef.com/ConsultantReqService/V2/ConsultantCDHReqPS?wsdl";
        String namespace = "http://www.pamperedchef.com/ConsultantSvc/V2";
        String serviceName = "ConsultantSvc";
        QName serviceQN = new QName(namespace, serviceName);
        try {
            Service.create(new URL(wsdlURL), serviceQN);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getTheWSDLTestC(){
        String wsdlURL = "https://fmwuat.pamperedchef.com:443/ConsultantReqService/V2/ConsultantCDHReqPS?wsdl";
        String namespace = "http://www.pamperedchef.com/ConsultantSvc/V2";
        String serviceName = "ConsultantSvc";
        QName serviceQN = new QName(namespace, serviceName);
        try {
            Service.create(new URL(wsdlURL), serviceQN);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
