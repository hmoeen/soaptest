
package com.pamperedchef.createconsultant.v2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hybrisUserID">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="globalID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Country">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OracleUserID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Title" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Mr."/>
 *               &lt;enumeration value="Mrs."/>
 *               &lt;enumeration value="Ms."/>
 *               &lt;enumeration value="Fr."/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Firstname">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="150"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MiddleName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Lastname">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="150"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Preferredname" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="80"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Suffix" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Jr."/>
 *               &lt;enumeration value="Sr."/>
 *               &lt;enumeration value="I"/>
 *               &lt;enumeration value="II"/>
 *               &lt;enumeration value="III"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="eMail">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DOB" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="PhoneNumbers" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PhoneNumber" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="hybrisPhoneID" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="240"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="PhoneType">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="Day"/>
 *                                   &lt;enumeration value="Night"/>
 *                                   &lt;enumeration value="Cell"/>
 *                                   &lt;enumeration value="Other"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="Number">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="1"/>
 *                                   &lt;maxLength value="40"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DefaultPhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Addresses" type="{http://www.pamperedchef.com/CreateConsultant/V2}AddressType"/>
 *         &lt;element name="DefaultShippingAddressID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DefaultBillingAddressID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DefaultMailingAddressID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PrimaryLanguage" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="en"/>
 *               &lt;enumeration value="es"/>
 *               &lt;enumeration value="de"/>
 *               &lt;enumeration value="fr"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SecondaryLanguage" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="en"/>
 *               &lt;enumeration value="es"/>
 *               &lt;enumeration value="de"/>
 *               &lt;enumeration value="fr"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ConsultantSSN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="9"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GovernmentIDType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="ssn"/>
 *               &lt;enumeration value="itin"/>
 *               &lt;enumeration value="sin"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Gender" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="MALE"/>
 *               &lt;enumeration value="FEMALE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ManagementLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pwsUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pwsActiveFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="pwsCancelFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="pwsExpirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="pwsAutoRenew" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="pwsBillingCycle" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RecruiterConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RecruiterCountryCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="KitCreditConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="KitCreditAuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="KitCreditClubName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="KITCREDIT"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="QuickStartConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuickStarterKitAuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuickStarterKitClubName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hybrisUserID",
    "globalID",
    "country",
    "oracleUserID",
    "title",
    "firstname",
    "middleName",
    "lastname",
    "preferredname",
    "suffix",
    "eMail",
    "dob",
    "phoneNumbers",
    "defaultPhoneID",
    "addresses",
    "defaultShippingAddressID",
    "defaultBillingAddressID",
    "defaultMailingAddressID",
    "primaryLanguage",
    "secondaryLanguage",
    "consultantSSN",
    "governmentIDType",
    "gender",
    "managementLevel",
    "pwsUrl",
    "pwsActiveFlag",
    "pwsCancelFlag",
    "pwsExpirationDate",
    "pwsAutoRenew",
    "pwsBillingCycle",
    "recruiterConsultantNumber",
    "recruiterCountryCd",
    "kitCreditConsultantNumber",
    "kitCreditAuthorizationCode",
    "kitCreditClubName",
    "quickStartConsultantNumber",
    "quickStarterKitAuthorizationCode",
    "quickStarterKitClubName"
})
@XmlRootElement(name = "Consultant")
public class Consultant {

    @XmlElement(required = true)
    protected String hybrisUserID;
    protected String globalID;
    @XmlElement(name = "Country", required = true)
    protected String country;
    @XmlElement(name = "OracleUserID")
    protected String oracleUserID;
    @XmlElement(name = "Title")
    protected String title;
    @XmlElement(name = "Firstname", required = true)
    protected String firstname;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "Lastname", required = true)
    protected String lastname;
    @XmlElement(name = "Preferredname")
    protected String preferredname;
    @XmlElement(name = "Suffix")
    protected String suffix;
    @XmlElement(required = true)
    protected String eMail;
    @XmlElement(name = "DOB")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dob;
    @XmlElement(name = "PhoneNumbers")
    protected Consultant.PhoneNumbers phoneNumbers;
    @XmlElement(name = "DefaultPhoneID")
    protected String defaultPhoneID;
    @XmlElement(name = "Addresses", required = true)
    protected AddressType addresses;
    @XmlElement(name = "DefaultShippingAddressID")
    protected String defaultShippingAddressID;
    @XmlElement(name = "DefaultBillingAddressID")
    protected String defaultBillingAddressID;
    @XmlElement(name = "DefaultMailingAddressID")
    protected String defaultMailingAddressID;
    @XmlElement(name = "PrimaryLanguage")
    protected String primaryLanguage;
    @XmlElement(name = "SecondaryLanguage")
    protected String secondaryLanguage;
    @XmlElement(name = "ConsultantSSN", required = true)
    protected String consultantSSN;
    @XmlElement(name = "GovernmentIDType")
    protected String governmentIDType;
    @XmlElement(name = "Gender")
    protected String gender;
    @XmlElement(name = "ManagementLevel")
    protected String managementLevel;
    protected String pwsUrl;
    protected Boolean pwsActiveFlag;
    protected Boolean pwsCancelFlag;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar pwsExpirationDate;
    protected Boolean pwsAutoRenew;
    protected Integer pwsBillingCycle;
    @XmlElement(name = "RecruiterConsultantNumber", required = true)
    protected String recruiterConsultantNumber;
    @XmlElement(name = "RecruiterCountryCd", required = true)
    protected String recruiterCountryCd;
    @XmlElement(name = "KitCreditConsultantNumber")
    protected String kitCreditConsultantNumber;
    @XmlElement(name = "KitCreditAuthorizationCode")
    protected String kitCreditAuthorizationCode;
    @XmlElement(name = "KitCreditClubName")
    protected String kitCreditClubName;
    @XmlElement(name = "QuickStartConsultantNumber")
    protected String quickStartConsultantNumber;
    @XmlElement(name = "QuickStarterKitAuthorizationCode")
    protected String quickStarterKitAuthorizationCode;
    @XmlElement(name = "QuickStarterKitClubName")
    protected String quickStarterKitClubName;

    /**
     * Gets the value of the hybrisUserID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHybrisUserID() {
        return hybrisUserID;
    }

    /**
     * Sets the value of the hybrisUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHybrisUserID(String value) {
        this.hybrisUserID = value;
    }

    /**
     * Gets the value of the globalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalID() {
        return globalID;
    }

    /**
     * Sets the value of the globalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalID(String value) {
        this.globalID = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the oracleUserID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOracleUserID() {
        return oracleUserID;
    }

    /**
     * Sets the value of the oracleUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOracleUserID(String value) {
        this.oracleUserID = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the firstname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Sets the value of the firstname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstname(String value) {
        this.firstname = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the lastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Sets the value of the lastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastname(String value) {
        this.lastname = value;
    }

    /**
     * Gets the value of the preferredname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredname() {
        return preferredname;
    }

    /**
     * Sets the value of the preferredname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredname(String value) {
        this.preferredname = value;
    }

    /**
     * Gets the value of the suffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Sets the value of the suffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuffix(String value) {
        this.suffix = value;
    }

    /**
     * Gets the value of the eMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMail() {
        return eMail;
    }

    /**
     * Sets the value of the eMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMail(String value) {
        this.eMail = value;
    }

    /**
     * Gets the value of the dob property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDOB() {
        return dob;
    }

    /**
     * Sets the value of the dob property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDOB(XMLGregorianCalendar value) {
        this.dob = value;
    }

    /**
     * Gets the value of the phoneNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link Consultant.PhoneNumbers }
     *     
     */
    public Consultant.PhoneNumbers getPhoneNumbers() {
        return phoneNumbers;
    }

    /**
     * Sets the value of the phoneNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Consultant.PhoneNumbers }
     *     
     */
    public void setPhoneNumbers(Consultant.PhoneNumbers value) {
        this.phoneNumbers = value;
    }

    /**
     * Gets the value of the defaultPhoneID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultPhoneID() {
        return defaultPhoneID;
    }

    /**
     * Sets the value of the defaultPhoneID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultPhoneID(String value) {
        this.defaultPhoneID = value;
    }

    /**
     * Gets the value of the addresses property.
     * 
     * @return
     *     possible object is
     *     {@link AddressType }
     *     
     */
    public AddressType getAddresses() {
        return addresses;
    }

    /**
     * Sets the value of the addresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressType }
     *     
     */
    public void setAddresses(AddressType value) {
        this.addresses = value;
    }

    /**
     * Gets the value of the defaultShippingAddressID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultShippingAddressID() {
        return defaultShippingAddressID;
    }

    /**
     * Sets the value of the defaultShippingAddressID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultShippingAddressID(String value) {
        this.defaultShippingAddressID = value;
    }

    /**
     * Gets the value of the defaultBillingAddressID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultBillingAddressID() {
        return defaultBillingAddressID;
    }

    /**
     * Sets the value of the defaultBillingAddressID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultBillingAddressID(String value) {
        this.defaultBillingAddressID = value;
    }

    /**
     * Gets the value of the defaultMailingAddressID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultMailingAddressID() {
        return defaultMailingAddressID;
    }

    /**
     * Sets the value of the defaultMailingAddressID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultMailingAddressID(String value) {
        this.defaultMailingAddressID = value;
    }

    /**
     * Gets the value of the primaryLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLanguage() {
        return primaryLanguage;
    }

    /**
     * Sets the value of the primaryLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLanguage(String value) {
        this.primaryLanguage = value;
    }

    /**
     * Gets the value of the secondaryLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryLanguage() {
        return secondaryLanguage;
    }

    /**
     * Sets the value of the secondaryLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryLanguage(String value) {
        this.secondaryLanguage = value;
    }

    /**
     * Gets the value of the consultantSSN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsultantSSN() {
        return consultantSSN;
    }

    /**
     * Sets the value of the consultantSSN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsultantSSN(String value) {
        this.consultantSSN = value;
    }

    /**
     * Gets the value of the governmentIDType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovernmentIDType() {
        return governmentIDType;
    }

    /**
     * Sets the value of the governmentIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovernmentIDType(String value) {
        this.governmentIDType = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the managementLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManagementLevel() {
        return managementLevel;
    }

    /**
     * Sets the value of the managementLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManagementLevel(String value) {
        this.managementLevel = value;
    }

    /**
     * Gets the value of the pwsUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPwsUrl() {
        return pwsUrl;
    }

    /**
     * Sets the value of the pwsUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPwsUrl(String value) {
        this.pwsUrl = value;
    }

    /**
     * Gets the value of the pwsActiveFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPwsActiveFlag() {
        return pwsActiveFlag;
    }

    /**
     * Sets the value of the pwsActiveFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPwsActiveFlag(Boolean value) {
        this.pwsActiveFlag = value;
    }

    /**
     * Gets the value of the pwsCancelFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPwsCancelFlag() {
        return pwsCancelFlag;
    }

    /**
     * Sets the value of the pwsCancelFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPwsCancelFlag(Boolean value) {
        this.pwsCancelFlag = value;
    }

    /**
     * Gets the value of the pwsExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPwsExpirationDate() {
        return pwsExpirationDate;
    }

    /**
     * Sets the value of the pwsExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPwsExpirationDate(XMLGregorianCalendar value) {
        this.pwsExpirationDate = value;
    }

    /**
     * Gets the value of the pwsAutoRenew property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPwsAutoRenew() {
        return pwsAutoRenew;
    }

    /**
     * Sets the value of the pwsAutoRenew property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPwsAutoRenew(Boolean value) {
        this.pwsAutoRenew = value;
    }

    /**
     * Gets the value of the pwsBillingCycle property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPwsBillingCycle() {
        return pwsBillingCycle;
    }

    /**
     * Sets the value of the pwsBillingCycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPwsBillingCycle(Integer value) {
        this.pwsBillingCycle = value;
    }

    /**
     * Gets the value of the recruiterConsultantNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecruiterConsultantNumber() {
        return recruiterConsultantNumber;
    }

    /**
     * Sets the value of the recruiterConsultantNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecruiterConsultantNumber(String value) {
        this.recruiterConsultantNumber = value;
    }

    /**
     * Gets the value of the recruiterCountryCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecruiterCountryCd() {
        return recruiterCountryCd;
    }

    /**
     * Sets the value of the recruiterCountryCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecruiterCountryCd(String value) {
        this.recruiterCountryCd = value;
    }

    /**
     * Gets the value of the kitCreditConsultantNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKitCreditConsultantNumber() {
        return kitCreditConsultantNumber;
    }

    /**
     * Sets the value of the kitCreditConsultantNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKitCreditConsultantNumber(String value) {
        this.kitCreditConsultantNumber = value;
    }

    /**
     * Gets the value of the kitCreditAuthorizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKitCreditAuthorizationCode() {
        return kitCreditAuthorizationCode;
    }

    /**
     * Sets the value of the kitCreditAuthorizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKitCreditAuthorizationCode(String value) {
        this.kitCreditAuthorizationCode = value;
    }

    /**
     * Gets the value of the kitCreditClubName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKitCreditClubName() {
        return kitCreditClubName;
    }

    /**
     * Sets the value of the kitCreditClubName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKitCreditClubName(String value) {
        this.kitCreditClubName = value;
    }

    /**
     * Gets the value of the quickStartConsultantNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuickStartConsultantNumber() {
        return quickStartConsultantNumber;
    }

    /**
     * Sets the value of the quickStartConsultantNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuickStartConsultantNumber(String value) {
        this.quickStartConsultantNumber = value;
    }

    /**
     * Gets the value of the quickStarterKitAuthorizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuickStarterKitAuthorizationCode() {
        return quickStarterKitAuthorizationCode;
    }

    /**
     * Sets the value of the quickStarterKitAuthorizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuickStarterKitAuthorizationCode(String value) {
        this.quickStarterKitAuthorizationCode = value;
    }

    /**
     * Gets the value of the quickStarterKitClubName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuickStarterKitClubName() {
        return quickStarterKitClubName;
    }

    /**
     * Sets the value of the quickStarterKitClubName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuickStarterKitClubName(String value) {
        this.quickStarterKitClubName = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PhoneNumber" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="hybrisPhoneID" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="240"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="PhoneType">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="Day"/>
     *                         &lt;enumeration value="Night"/>
     *                         &lt;enumeration value="Cell"/>
     *                         &lt;enumeration value="Other"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="Number">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;minLength value="1"/>
     *                         &lt;maxLength value="40"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "phoneNumber"
    })
    public static class PhoneNumbers {

        @XmlElement(name = "PhoneNumber", required = true)
        protected List<Consultant.PhoneNumbers.PhoneNumber> phoneNumber;

        /**
         * Gets the value of the phoneNumber property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the phoneNumber property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPhoneNumber().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Consultant.PhoneNumbers.PhoneNumber }
         * 
         * 
         */
        public List<Consultant.PhoneNumbers.PhoneNumber> getPhoneNumber() {
            if (phoneNumber == null) {
                phoneNumber = new ArrayList<Consultant.PhoneNumbers.PhoneNumber>();
            }
            return this.phoneNumber;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="hybrisPhoneID" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="240"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="PhoneType">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="Day"/>
         *               &lt;enumeration value="Night"/>
         *               &lt;enumeration value="Cell"/>
         *               &lt;enumeration value="Other"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="Number">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;minLength value="1"/>
         *               &lt;maxLength value="40"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "hybrisPhoneID",
            "phoneType",
            "number",
            "isTextable"
        })
        public static class PhoneNumber {

            protected String hybrisPhoneID;
            @XmlElement(name = "PhoneType", required = true)
            protected String phoneType;
            @XmlElement(name = "Number", required = true)
            protected String number;
            @XmlElement(name = "IsTextable")
            protected boolean isTextable;

            /**
             * Gets the value of the hybrisPhoneID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHybrisPhoneID() {
                return hybrisPhoneID;
            }

            /**
             * Sets the value of the hybrisPhoneID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHybrisPhoneID(String value) {
                this.hybrisPhoneID = value;
            }

            /**
             * Gets the value of the phoneType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneType() {
                return phoneType;
            }

            /**
             * Sets the value of the phoneType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneType(String value) {
                this.phoneType = value;
            }

            /**
             * Gets the value of the number property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumber() {
                return number;
            }

            /**
             * Sets the value of the number property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumber(String value) {
                this.number = value;
            }

            /**
             * Gets the value of the isTextable property.
             * 
             */
            public boolean isIsTextable() {
                return isTextable;
            }

            /**
             * Sets the value of the isTextable property.
             * 
             */
            public void setIsTextable(boolean value) {
                this.isTextable = value;
            }

        }

    }

}
