
package com.pamperedchef.deleteuseraddressresponse;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserAddresses" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="hybrisUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="oracleUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="hybrisAddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="oracleAddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userAddresses",
    "code",
    "message"
})
@XmlRootElement(name = "Response")
public class Response {

    @XmlElement(name = "UserAddresses", required = true)
    protected List<Response.UserAddresses> userAddresses;
    protected int code;
    @XmlElement(required = true)
    protected String message;

    /**
     * Gets the value of the userAddresses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userAddresses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserAddresses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Response.UserAddresses }
     * 
     * 
     */
    public List<Response.UserAddresses> getUserAddresses() {
        if (userAddresses == null) {
            userAddresses = new ArrayList<Response.UserAddresses>();
        }
        return this.userAddresses;
    }

    /**
     * Gets the value of the code property.
     * 
     */
    public int getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     */
    public void setCode(int value) {
        this.code = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }


    /**
     * 
     *     							Unique ConsultantID (CDH)
     *     						
     * 
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="hybrisUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="oracleUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="hybrisAddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="oracleAddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hybrisUserID",
        "oracleUserID",
        "hybrisAddressID",
        "oracleAddressID",
        "code",
        "message"
    })
    public static class UserAddresses {

        @XmlElement(required = true)
        protected String hybrisUserID;
        @XmlElement(required = true)
        protected String oracleUserID;
        @XmlElement(required = true)
        protected String hybrisAddressID;
        @XmlElement(required = true)
        protected String oracleAddressID;
        @XmlElement(name = "Code")
        protected int code;
        @XmlElement(name = "Message", required = true)
        protected String message;

        /**
         * Gets the value of the hybrisUserID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHybrisUserID() {
            return hybrisUserID;
        }

        /**
         * Sets the value of the hybrisUserID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHybrisUserID(String value) {
            this.hybrisUserID = value;
        }

        /**
         * Gets the value of the oracleUserID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOracleUserID() {
            return oracleUserID;
        }

        /**
         * Sets the value of the oracleUserID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOracleUserID(String value) {
            this.oracleUserID = value;
        }

        /**
         * Gets the value of the hybrisAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHybrisAddressID() {
            return hybrisAddressID;
        }

        /**
         * Sets the value of the hybrisAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHybrisAddressID(String value) {
            this.hybrisAddressID = value;
        }

        /**
         * Gets the value of the oracleAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOracleAddressID() {
            return oracleAddressID;
        }

        /**
         * Sets the value of the oracleAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOracleAddressID(String value) {
            this.oracleAddressID = value;
        }

        /**
         * Gets the value of the code property.
         * 
         */
        public int getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         */
        public void setCode(int value) {
            this.code = value;
        }

        /**
         * Gets the value of the message property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMessage() {
            return message;
        }

        /**
         * Sets the value of the message property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMessage(String value) {
            this.message = value;
        }

    }

}
