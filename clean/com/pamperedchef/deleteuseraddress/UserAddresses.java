
package com.pamperedchef.deleteuseraddress;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserAddress" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="hybrisUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="globalID" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="1"/>
 *                         &lt;maxLength value="60"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Country">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="1"/>
 *                         &lt;maxLength value="60"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="OracleUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="hybrisAddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="oracleAddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userAddress"
})
@XmlRootElement(name = "UserAddresses")
public class UserAddresses {

    @XmlElement(name = "UserAddress", required = true)
    protected List<UserAddresses.UserAddress> userAddress;

    /**
     * Gets the value of the userAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserAddresses.UserAddress }
     * 
     * 
     */
    public List<UserAddresses.UserAddress> getUserAddress() {
        if (userAddress == null) {
            userAddress = new ArrayList<UserAddresses.UserAddress>();
        }
        return this.userAddress;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="hybrisUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="globalID" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="1"/>
     *               &lt;maxLength value="60"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Country">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="1"/>
     *               &lt;maxLength value="60"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="OracleUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="hybrisAddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="oracleAddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hybrisUserID",
        "globalID",
        "country",
        "oracleUserID",
        "hybrisAddressID",
        "oracleAddressID"
    })
    public static class UserAddress {

        @XmlElement(required = true)
        protected String hybrisUserID;
        protected String globalID;
        @XmlElement(name = "Country", required = true)
        protected String country;
        @XmlElement(name = "OracleUserID", required = true)
        protected String oracleUserID;
        @XmlElement(required = true)
        protected String hybrisAddressID;
        @XmlElement(required = true)
        protected String oracleAddressID;

        /**
         * Gets the value of the hybrisUserID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHybrisUserID() {
            return hybrisUserID;
        }

        /**
         * Sets the value of the hybrisUserID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHybrisUserID(String value) {
            this.hybrisUserID = value;
        }

        /**
         * Gets the value of the globalID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGlobalID() {
            return globalID;
        }

        /**
         * Sets the value of the globalID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGlobalID(String value) {
            this.globalID = value;
        }

        /**
         * Gets the value of the country property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountry() {
            return country;
        }

        /**
         * Sets the value of the country property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountry(String value) {
            this.country = value;
        }

        /**
         * Gets the value of the oracleUserID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOracleUserID() {
            return oracleUserID;
        }

        /**
         * Sets the value of the oracleUserID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOracleUserID(String value) {
            this.oracleUserID = value;
        }

        /**
         * Gets the value of the hybrisAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHybrisAddressID() {
            return hybrisAddressID;
        }

        /**
         * Sets the value of the hybrisAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHybrisAddressID(String value) {
            this.hybrisAddressID = value;
        }

        /**
         * Gets the value of the oracleAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOracleAddressID() {
            return oracleAddressID;
        }

        /**
         * Sets the value of the oracleAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOracleAddressID(String value) {
            this.oracleAddressID = value;
        }

    }

}
