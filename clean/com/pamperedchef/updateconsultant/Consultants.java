
package com.pamperedchef.updateconsultant;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Consultant" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="hybrisUserID">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="240"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="globalID" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="1"/>
 *                         &lt;maxLength value="60"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Country">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;minLength value="1"/>
 *                         &lt;maxLength value="60"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="OracleUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Title" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="Mr."/>
 *                         &lt;enumeration value="Mrs."/>
 *                         &lt;enumeration value="Ms."/>
 *                         &lt;enumeration value="Fr."/>
 *                         &lt;enumeration value=""/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Firstname">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="150"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="MiddleName" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="60"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Lastname">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="150"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Preferredname" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="80"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Suffix" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="Jr."/>
 *                         &lt;enumeration value="Sr."/>
 *                         &lt;enumeration value="I"/>
 *                         &lt;enumeration value="II"/>
 *                         &lt;enumeration value="III"/>
 *                         &lt;enumeration value=""/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="eMail">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="240"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DOB" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="PhoneNumbers" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PhoneNumber" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="hybrisPhoneID">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;maxLength value="240"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="PhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="PhoneType" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="Day"/>
 *                                             &lt;enumeration value="Night"/>
 *                                             &lt;enumeration value="Cell"/>
 *                                             &lt;enumeration value="Other"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="Number" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;maxLength value="40"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="DefaultPhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DefaultOraclePhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Addresses" type="{http://www.pamperedchef.com/UpdateConsultant}AddressType" minOccurs="0"/>
 *                   &lt;element name="DefaultShippingAddressID" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="240"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DefaultBillingAddressID" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="240"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DefaultMailingAddressID" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="240"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DefaultOracleShippingAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DefaultOracleBillingAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DefaultOracleMailingAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PrimaryLanguage" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="en"/>
 *                         &lt;enumeration value="es"/>
 *                         &lt;enumeration value="de"/>
 *                         &lt;enumeration value="fr"/>
 *                         &lt;enumeration value=""/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="SecondaryLanguage" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="en"/>
 *                         &lt;enumeration value="es"/>
 *                         &lt;enumeration value="de"/>
 *                         &lt;enumeration value="fr"/>
 *                         &lt;enumeration value=""/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="ConsultantNumber" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="30"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="GovernmentIDType" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="ssn"/>
 *                         &lt;enumeration value="itin"/>
 *                         &lt;enumeration value="sin"/>
 *                         &lt;enumeration value=""/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="ManagementLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RecruiterConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RecruiterCountryCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="QuickStartConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ConsultantStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DirectorAgreementDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="DirectorConfirmationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="Gender" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="MALE"/>
 *                         &lt;enumeration value="FEMALE"/>
 *                         &lt;enumeration value=""/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="CancelPWS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="AutoRenewPWS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="ActivePWS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="PWSExpirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="pwsURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LeadEligible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="KitEnhancementEligible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="ACHBankName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ACHAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ACHRoutingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ACHAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RenewConsultant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultant"
})
@XmlRootElement(name = "Consultants")
public class Consultants {

    @XmlElement(name = "Consultant", required = true)
    protected List<Consultants.Consultant> consultant;

    /**
     * Gets the value of the consultant property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consultant property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsultant().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Consultants.Consultant }
     * 
     * 
     */
    public List<Consultants.Consultant> getConsultant() {
        if (consultant == null) {
            consultant = new ArrayList<Consultants.Consultant>();
        }
        return this.consultant;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="hybrisUserID">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="240"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="globalID" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="1"/>
     *               &lt;maxLength value="60"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Country">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;minLength value="1"/>
     *               &lt;maxLength value="60"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="OracleUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Title" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="Mr."/>
     *               &lt;enumeration value="Mrs."/>
     *               &lt;enumeration value="Ms."/>
     *               &lt;enumeration value="Fr."/>
     *               &lt;enumeration value=""/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Firstname">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="150"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="MiddleName" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="60"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Lastname">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="150"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Preferredname" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="80"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Suffix" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="Jr."/>
     *               &lt;enumeration value="Sr."/>
     *               &lt;enumeration value="I"/>
     *               &lt;enumeration value="II"/>
     *               &lt;enumeration value="III"/>
     *               &lt;enumeration value=""/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="eMail">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="240"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="DOB" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="PhoneNumbers" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PhoneNumber" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="hybrisPhoneID">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;maxLength value="240"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="PhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="PhoneType" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="Day"/>
     *                                   &lt;enumeration value="Night"/>
     *                                   &lt;enumeration value="Cell"/>
     *                                   &lt;enumeration value="Other"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="Number" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;maxLength value="40"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="DefaultPhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DefaultOraclePhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Addresses" type="{http://www.pamperedchef.com/UpdateConsultant}AddressType" minOccurs="0"/>
     *         &lt;element name="DefaultShippingAddressID" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="240"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="DefaultBillingAddressID" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="240"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="DefaultMailingAddressID" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="240"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="DefaultOracleShippingAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DefaultOracleBillingAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DefaultOracleMailingAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PrimaryLanguage" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="en"/>
     *               &lt;enumeration value="es"/>
     *               &lt;enumeration value="de"/>
     *               &lt;enumeration value="fr"/>
     *               &lt;enumeration value=""/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="SecondaryLanguage" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="en"/>
     *               &lt;enumeration value="es"/>
     *               &lt;enumeration value="de"/>
     *               &lt;enumeration value="fr"/>
     *               &lt;enumeration value=""/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="ConsultantNumber" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="30"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="GovernmentIDType" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="ssn"/>
     *               &lt;enumeration value="itin"/>
     *               &lt;enumeration value="sin"/>
     *               &lt;enumeration value=""/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="ManagementLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RecruiterConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RecruiterCountryCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="QuickStartConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ConsultantStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DirectorAgreementDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="DirectorConfirmationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="Gender" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="MALE"/>
     *               &lt;enumeration value="FEMALE"/>
     *               &lt;enumeration value=""/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="CancelPWS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="AutoRenewPWS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="ActivePWS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="PWSExpirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="pwsURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LeadEligible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="KitEnhancementEligible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="ACHBankName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ACHAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ACHRoutingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ACHAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RenewConsultant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hybrisUserID",
        "globalID",
        "country",
        "oracleUserID",
        "title",
        "firstname",
        "middleName",
        "lastname",
        "preferredname",
        "suffix",
        "eMail",
        "dob",
        "phoneNumbers",
        "defaultPhoneID",
        "defaultOraclePhoneID",
        "addresses",
        "defaultShippingAddressID",
        "defaultBillingAddressID",
        "defaultMailingAddressID",
        "defaultOracleShippingAddressID",
        "defaultOracleBillingAddressID",
        "defaultOracleMailingAddressID",
        "primaryLanguage",
        "secondaryLanguage",
        "consultantNumber",
        "governmentIDType",
        "managementLevel",
        "recruiterConsultantNumber",
        "recruiterCountryCd",
        "quickStartConsultantNumber",
        "consultantStatus",
        "directorAgreementDate",
        "directorConfirmationDate",
        "gender",
        "cancelPWS",
        "autoRenewPWS",
        "activePWS",
        "pwsExpirationDate",
        "pwsURL",
        "leadEligible",
        "kitEnhancementEligible",
        "achBankName",
        "achAccountType",
        "achRoutingNumber",
        "achAccountNumber",
        "renewConsultant"
    })
    public static class Consultant {

        @XmlElement(required = true)
        protected String hybrisUserID;
        protected String globalID;
        @XmlElement(name = "Country", required = true)
        protected String country;
        @XmlElement(name = "OracleUserID", required = true)
        protected String oracleUserID;
        @XmlElement(name = "Title")
        protected String title;
        @XmlElement(name = "Firstname", required = true)
        protected String firstname;
        @XmlElement(name = "MiddleName")
        protected String middleName;
        @XmlElement(name = "Lastname", required = true)
        protected String lastname;
        @XmlElement(name = "Preferredname")
        protected String preferredname;
        @XmlElement(name = "Suffix")
        protected String suffix;
        @XmlElement(required = true)
        protected String eMail;
        @XmlElementRef(name = "DOB", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> dob;
        @XmlElement(name = "PhoneNumbers")
        protected Consultants.Consultant.PhoneNumbers phoneNumbers;
        @XmlElement(name = "DefaultPhoneID")
        protected String defaultPhoneID;
        @XmlElement(name = "DefaultOraclePhoneID")
        protected String defaultOraclePhoneID;
        @XmlElement(name = "Addresses")
        protected AddressType addresses;
        @XmlElement(name = "DefaultShippingAddressID")
        protected String defaultShippingAddressID;
        @XmlElement(name = "DefaultBillingAddressID")
        protected String defaultBillingAddressID;
        @XmlElement(name = "DefaultMailingAddressID")
        protected String defaultMailingAddressID;
        @XmlElement(name = "DefaultOracleShippingAddressID")
        protected String defaultOracleShippingAddressID;
        @XmlElement(name = "DefaultOracleBillingAddressID")
        protected String defaultOracleBillingAddressID;
        @XmlElement(name = "DefaultOracleMailingAddressID")
        protected String defaultOracleMailingAddressID;
        @XmlElement(name = "PrimaryLanguage")
        protected String primaryLanguage;
        @XmlElement(name = "SecondaryLanguage")
        protected String secondaryLanguage;
        @XmlElement(name = "ConsultantNumber")
        protected String consultantNumber;
        @XmlElement(name = "GovernmentIDType")
        protected String governmentIDType;
        @XmlElement(name = "ManagementLevel")
        protected String managementLevel;
        @XmlElement(name = "RecruiterConsultantNumber")
        protected String recruiterConsultantNumber;
        @XmlElement(name = "RecruiterCountryCd")
        protected String recruiterCountryCd;
        @XmlElement(name = "QuickStartConsultantNumber")
        protected String quickStartConsultantNumber;
        @XmlElement(name = "ConsultantStatus")
        protected String consultantStatus;
        @XmlElementRef(name = "DirectorAgreementDate", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> directorAgreementDate;
        @XmlElementRef(name = "DirectorConfirmationDate", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> directorConfirmationDate;
        @XmlElement(name = "Gender")
        protected String gender;
        @XmlElementRef(name = "CancelPWS", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<Boolean> cancelPWS;
        @XmlElementRef(name = "AutoRenewPWS", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<Boolean> autoRenewPWS;
        @XmlElementRef(name = "ActivePWS", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<Boolean> activePWS;
        @XmlElementRef(name = "PWSExpirationDate", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<XMLGregorianCalendar> pwsExpirationDate;
        protected String pwsURL;
        @XmlElementRef(name = "LeadEligible", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<Boolean> leadEligible;
        @XmlElementRef(name = "KitEnhancementEligible", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<Boolean> kitEnhancementEligible;
        @XmlElement(name = "ACHBankName")
        protected String achBankName;
        @XmlElement(name = "ACHAccountType")
        protected String achAccountType;
        @XmlElement(name = "ACHRoutingNumber")
        protected String achRoutingNumber;
        @XmlElement(name = "ACHAccountNumber")
        protected String achAccountNumber;
        @XmlElementRef(name = "RenewConsultant", namespace = "http://www.pamperedchef.com/UpdateConsultant", type = JAXBElement.class, required = false)
        protected JAXBElement<Boolean> renewConsultant;

        /**
         * Gets the value of the hybrisUserID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHybrisUserID() {
            return hybrisUserID;
        }

        /**
         * Sets the value of the hybrisUserID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHybrisUserID(String value) {
            this.hybrisUserID = value;
        }

        /**
         * Gets the value of the globalID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGlobalID() {
            return globalID;
        }

        /**
         * Sets the value of the globalID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGlobalID(String value) {
            this.globalID = value;
        }

        /**
         * Gets the value of the country property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountry() {
            return country;
        }

        /**
         * Sets the value of the country property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountry(String value) {
            this.country = value;
        }

        /**
         * Gets the value of the oracleUserID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOracleUserID() {
            return oracleUserID;
        }

        /**
         * Sets the value of the oracleUserID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOracleUserID(String value) {
            this.oracleUserID = value;
        }

        /**
         * Gets the value of the title property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTitle() {
            return title;
        }

        /**
         * Sets the value of the title property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTitle(String value) {
            this.title = value;
        }

        /**
         * Gets the value of the firstname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFirstname() {
            return firstname;
        }

        /**
         * Sets the value of the firstname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFirstname(String value) {
            this.firstname = value;
        }

        /**
         * Gets the value of the middleName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMiddleName() {
            return middleName;
        }

        /**
         * Sets the value of the middleName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMiddleName(String value) {
            this.middleName = value;
        }

        /**
         * Gets the value of the lastname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLastname() {
            return lastname;
        }

        /**
         * Sets the value of the lastname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLastname(String value) {
            this.lastname = value;
        }

        /**
         * Gets the value of the preferredname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreferredname() {
            return preferredname;
        }

        /**
         * Sets the value of the preferredname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreferredname(String value) {
            this.preferredname = value;
        }

        /**
         * Gets the value of the suffix property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuffix() {
            return suffix;
        }

        /**
         * Sets the value of the suffix property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuffix(String value) {
            this.suffix = value;
        }

        /**
         * Gets the value of the eMail property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEMail() {
            return eMail;
        }

        /**
         * Sets the value of the eMail property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEMail(String value) {
            this.eMail = value;
        }

        /**
         * Gets the value of the dob property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getDOB() {
            return dob;
        }

        /**
         * Sets the value of the dob property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setDOB(JAXBElement<XMLGregorianCalendar> value) {
            this.dob = value;
        }

        /**
         * Gets the value of the phoneNumbers property.
         * 
         * @return
         *     possible object is
         *     {@link Consultants.Consultant.PhoneNumbers }
         *     
         */
        public Consultants.Consultant.PhoneNumbers getPhoneNumbers() {
            return phoneNumbers;
        }

        /**
         * Sets the value of the phoneNumbers property.
         * 
         * @param value
         *     allowed object is
         *     {@link Consultants.Consultant.PhoneNumbers }
         *     
         */
        public void setPhoneNumbers(Consultants.Consultant.PhoneNumbers value) {
            this.phoneNumbers = value;
        }

        /**
         * Gets the value of the defaultPhoneID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefaultPhoneID() {
            return defaultPhoneID;
        }

        /**
         * Sets the value of the defaultPhoneID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefaultPhoneID(String value) {
            this.defaultPhoneID = value;
        }

        /**
         * Gets the value of the defaultOraclePhoneID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefaultOraclePhoneID() {
            return defaultOraclePhoneID;
        }

        /**
         * Sets the value of the defaultOraclePhoneID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefaultOraclePhoneID(String value) {
            this.defaultOraclePhoneID = value;
        }

        /**
         * Gets the value of the addresses property.
         * 
         * @return
         *     possible object is
         *     {@link AddressType }
         *     
         */
        public AddressType getAddresses() {
            return addresses;
        }

        /**
         * Sets the value of the addresses property.
         * 
         * @param value
         *     allowed object is
         *     {@link AddressType }
         *     
         */
        public void setAddresses(AddressType value) {
            this.addresses = value;
        }

        /**
         * Gets the value of the defaultShippingAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefaultShippingAddressID() {
            return defaultShippingAddressID;
        }

        /**
         * Sets the value of the defaultShippingAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefaultShippingAddressID(String value) {
            this.defaultShippingAddressID = value;
        }

        /**
         * Gets the value of the defaultBillingAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefaultBillingAddressID() {
            return defaultBillingAddressID;
        }

        /**
         * Sets the value of the defaultBillingAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefaultBillingAddressID(String value) {
            this.defaultBillingAddressID = value;
        }

        /**
         * Gets the value of the defaultMailingAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefaultMailingAddressID() {
            return defaultMailingAddressID;
        }

        /**
         * Sets the value of the defaultMailingAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefaultMailingAddressID(String value) {
            this.defaultMailingAddressID = value;
        }

        /**
         * Gets the value of the defaultOracleShippingAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefaultOracleShippingAddressID() {
            return defaultOracleShippingAddressID;
        }

        /**
         * Sets the value of the defaultOracleShippingAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefaultOracleShippingAddressID(String value) {
            this.defaultOracleShippingAddressID = value;
        }

        /**
         * Gets the value of the defaultOracleBillingAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefaultOracleBillingAddressID() {
            return defaultOracleBillingAddressID;
        }

        /**
         * Sets the value of the defaultOracleBillingAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefaultOracleBillingAddressID(String value) {
            this.defaultOracleBillingAddressID = value;
        }

        /**
         * Gets the value of the defaultOracleMailingAddressID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefaultOracleMailingAddressID() {
            return defaultOracleMailingAddressID;
        }

        /**
         * Sets the value of the defaultOracleMailingAddressID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefaultOracleMailingAddressID(String value) {
            this.defaultOracleMailingAddressID = value;
        }

        /**
         * Gets the value of the primaryLanguage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrimaryLanguage() {
            return primaryLanguage;
        }

        /**
         * Sets the value of the primaryLanguage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrimaryLanguage(String value) {
            this.primaryLanguage = value;
        }

        /**
         * Gets the value of the secondaryLanguage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSecondaryLanguage() {
            return secondaryLanguage;
        }

        /**
         * Sets the value of the secondaryLanguage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSecondaryLanguage(String value) {
            this.secondaryLanguage = value;
        }

        /**
         * Gets the value of the consultantNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConsultantNumber() {
            return consultantNumber;
        }

        /**
         * Sets the value of the consultantNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConsultantNumber(String value) {
            this.consultantNumber = value;
        }

        /**
         * Gets the value of the governmentIDType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGovernmentIDType() {
            return governmentIDType;
        }

        /**
         * Sets the value of the governmentIDType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGovernmentIDType(String value) {
            this.governmentIDType = value;
        }

        /**
         * Gets the value of the managementLevel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getManagementLevel() {
            return managementLevel;
        }

        /**
         * Sets the value of the managementLevel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setManagementLevel(String value) {
            this.managementLevel = value;
        }

        /**
         * Gets the value of the recruiterConsultantNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecruiterConsultantNumber() {
            return recruiterConsultantNumber;
        }

        /**
         * Sets the value of the recruiterConsultantNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecruiterConsultantNumber(String value) {
            this.recruiterConsultantNumber = value;
        }

        /**
         * Gets the value of the recruiterCountryCd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecruiterCountryCd() {
            return recruiterCountryCd;
        }

        /**
         * Sets the value of the recruiterCountryCd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecruiterCountryCd(String value) {
            this.recruiterCountryCd = value;
        }

        /**
         * Gets the value of the quickStartConsultantNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuickStartConsultantNumber() {
            return quickStartConsultantNumber;
        }

        /**
         * Sets the value of the quickStartConsultantNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuickStartConsultantNumber(String value) {
            this.quickStartConsultantNumber = value;
        }

        /**
         * Gets the value of the consultantStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConsultantStatus() {
            return consultantStatus;
        }

        /**
         * Sets the value of the consultantStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConsultantStatus(String value) {
            this.consultantStatus = value;
        }

        /**
         * Gets the value of the directorAgreementDate property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getDirectorAgreementDate() {
            return directorAgreementDate;
        }

        /**
         * Sets the value of the directorAgreementDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setDirectorAgreementDate(JAXBElement<XMLGregorianCalendar> value) {
            this.directorAgreementDate = value;
        }

        /**
         * Gets the value of the directorConfirmationDate property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getDirectorConfirmationDate() {
            return directorConfirmationDate;
        }

        /**
         * Sets the value of the directorConfirmationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setDirectorConfirmationDate(JAXBElement<XMLGregorianCalendar> value) {
            this.directorConfirmationDate = value;
        }

        /**
         * Gets the value of the gender property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGender() {
            return gender;
        }

        /**
         * Sets the value of the gender property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGender(String value) {
            this.gender = value;
        }

        /**
         * Gets the value of the cancelPWS property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public JAXBElement<Boolean> getCancelPWS() {
            return cancelPWS;
        }

        /**
         * Sets the value of the cancelPWS property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public void setCancelPWS(JAXBElement<Boolean> value) {
            this.cancelPWS = value;
        }

        /**
         * Gets the value of the autoRenewPWS property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public JAXBElement<Boolean> getAutoRenewPWS() {
            return autoRenewPWS;
        }

        /**
         * Sets the value of the autoRenewPWS property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public void setAutoRenewPWS(JAXBElement<Boolean> value) {
            this.autoRenewPWS = value;
        }

        /**
         * Gets the value of the activePWS property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public JAXBElement<Boolean> getActivePWS() {
            return activePWS;
        }

        /**
         * Sets the value of the activePWS property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public void setActivePWS(JAXBElement<Boolean> value) {
            this.activePWS = value;
        }

        /**
         * Gets the value of the pwsExpirationDate property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public JAXBElement<XMLGregorianCalendar> getPWSExpirationDate() {
            return pwsExpirationDate;
        }

        /**
         * Sets the value of the pwsExpirationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
         *     
         */
        public void setPWSExpirationDate(JAXBElement<XMLGregorianCalendar> value) {
            this.pwsExpirationDate = value;
        }

        /**
         * Gets the value of the pwsURL property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPwsURL() {
            return pwsURL;
        }

        /**
         * Sets the value of the pwsURL property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPwsURL(String value) {
            this.pwsURL = value;
        }

        /**
         * Gets the value of the leadEligible property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public JAXBElement<Boolean> getLeadEligible() {
            return leadEligible;
        }

        /**
         * Sets the value of the leadEligible property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public void setLeadEligible(JAXBElement<Boolean> value) {
            this.leadEligible = value;
        }

        /**
         * Gets the value of the kitEnhancementEligible property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public JAXBElement<Boolean> getKitEnhancementEligible() {
            return kitEnhancementEligible;
        }

        /**
         * Sets the value of the kitEnhancementEligible property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public void setKitEnhancementEligible(JAXBElement<Boolean> value) {
            this.kitEnhancementEligible = value;
        }

        /**
         * Gets the value of the achBankName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACHBankName() {
            return achBankName;
        }

        /**
         * Sets the value of the achBankName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACHBankName(String value) {
            this.achBankName = value;
        }

        /**
         * Gets the value of the achAccountType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACHAccountType() {
            return achAccountType;
        }

        /**
         * Sets the value of the achAccountType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACHAccountType(String value) {
            this.achAccountType = value;
        }

        /**
         * Gets the value of the achRoutingNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACHRoutingNumber() {
            return achRoutingNumber;
        }

        /**
         * Sets the value of the achRoutingNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACHRoutingNumber(String value) {
            this.achRoutingNumber = value;
        }

        /**
         * Gets the value of the achAccountNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACHAccountNumber() {
            return achAccountNumber;
        }

        /**
         * Sets the value of the achAccountNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACHAccountNumber(String value) {
            this.achAccountNumber = value;
        }

        /**
         * Gets the value of the renewConsultant property.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public JAXBElement<Boolean> getRenewConsultant() {
            return renewConsultant;
        }

        /**
         * Sets the value of the renewConsultant property.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         *     
         */
        public void setRenewConsultant(JAXBElement<Boolean> value) {
            this.renewConsultant = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PhoneNumber" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="hybrisPhoneID">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;maxLength value="240"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="PhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="PhoneType" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="Day"/>
         *                         &lt;enumeration value="Night"/>
         *                         &lt;enumeration value="Cell"/>
         *                         &lt;enumeration value="Other"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="Number" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;maxLength value="40"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "phoneNumber"
        })
        public static class PhoneNumbers {

            @XmlElement(name = "PhoneNumber", required = true)
            protected List<Consultants.Consultant.PhoneNumbers.PhoneNumber> phoneNumber;

            /**
             * Gets the value of the phoneNumber property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the phoneNumber property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPhoneNumber().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Consultants.Consultant.PhoneNumbers.PhoneNumber }
             * 
             * 
             */
            public List<Consultants.Consultant.PhoneNumbers.PhoneNumber> getPhoneNumber() {
                if (phoneNumber == null) {
                    phoneNumber = new ArrayList<Consultants.Consultant.PhoneNumbers.PhoneNumber>();
                }
                return this.phoneNumber;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="hybrisPhoneID">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;maxLength value="240"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="PhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="PhoneType" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="Day"/>
             *               &lt;enumeration value="Night"/>
             *               &lt;enumeration value="Cell"/>
             *               &lt;enumeration value="Other"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="Number" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;maxLength value="40"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "hybrisPhoneID",
                "phoneID",
                "phoneType",
                "number",
                "isTextable"
            })
            public static class PhoneNumber {

                @XmlElement(required = true)
                protected String hybrisPhoneID;
                @XmlElement(name = "PhoneID")
                protected String phoneID;
                @XmlElement(name = "PhoneType")
                protected String phoneType;
                @XmlElement(name = "Number")
                protected String number;
                @XmlElement(name = "IsTextable", required = true, type = Boolean.class, nillable = true)
                protected Boolean isTextable;

                /**
                 * Gets the value of the hybrisPhoneID property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHybrisPhoneID() {
                    return hybrisPhoneID;
                }

                /**
                 * Sets the value of the hybrisPhoneID property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHybrisPhoneID(String value) {
                    this.hybrisPhoneID = value;
                }

                /**
                 * Gets the value of the phoneID property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneID() {
                    return phoneID;
                }

                /**
                 * Sets the value of the phoneID property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneID(String value) {
                    this.phoneID = value;
                }

                /**
                 * Gets the value of the phoneType property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneType() {
                    return phoneType;
                }

                /**
                 * Sets the value of the phoneType property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneType(String value) {
                    this.phoneType = value;
                }

                /**
                 * Gets the value of the number property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumber() {
                    return number;
                }

                /**
                 * Sets the value of the number property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumber(String value) {
                    this.number = value;
                }

                /**
                 * Gets the value of the isTextable property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isIsTextable() {
                    return isTextable;
                }

                /**
                 * Sets the value of the isTextable property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setIsTextable(Boolean value) {
                    this.isTextable = value;
                }

            }

        }

    }

}
