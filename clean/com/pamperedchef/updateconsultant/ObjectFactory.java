
package com.pamperedchef.updateconsultant;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.pamperedchef.updateconsultant package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultantsConsultantActivePWS_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "ActivePWS");
    private final static QName _ConsultantsConsultantRenewConsultant_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "RenewConsultant");
    private final static QName _ConsultantsConsultantKitEnhancementEligible_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "KitEnhancementEligible");
    private final static QName _ConsultantsConsultantDOB_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "DOB");
    private final static QName _ConsultantsConsultantLeadEligible_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "LeadEligible");
    private final static QName _ConsultantsConsultantDirectorAgreementDate_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "DirectorAgreementDate");
    private final static QName _ConsultantsConsultantDirectorConfirmationDate_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "DirectorConfirmationDate");
    private final static QName _ConsultantsConsultantPWSExpirationDate_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "PWSExpirationDate");
    private final static QName _ConsultantsConsultantCancelPWS_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "CancelPWS");
    private final static QName _ConsultantsConsultantAutoRenewPWS_QNAME = new QName("http://www.pamperedchef.com/UpdateConsultant", "AutoRenewPWS");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.pamperedchef.updateconsultant
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Consultants }
     * 
     */
    public Consultants createConsultants() {
        return new Consultants();
    }

    /**
     * Create an instance of {@link Consultants.Consultant }
     * 
     */
    public Consultants.Consultant createConsultantsConsultant() {
        return new Consultants.Consultant();
    }

    /**
     * Create an instance of {@link Consultants.Consultant.PhoneNumbers }
     * 
     */
    public Consultants.Consultant.PhoneNumbers createConsultantsConsultantPhoneNumbers() {
        return new Consultants.Consultant.PhoneNumbers();
    }

    /**
     * Create an instance of {@link AddressDetailType }
     * 
     */
    public AddressDetailType createAddressDetailType() {
        return new AddressDetailType();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link Consultants.Consultant.PhoneNumbers.PhoneNumber }
     * 
     */
    public Consultants.Consultant.PhoneNumbers.PhoneNumber createConsultantsConsultantPhoneNumbersPhoneNumber() {
        return new Consultants.Consultant.PhoneNumbers.PhoneNumber();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "ActivePWS", scope = Consultants.Consultant.class)
    public JAXBElement<Boolean> createConsultantsConsultantActivePWS(Boolean value) {
        return new JAXBElement<Boolean>(_ConsultantsConsultantActivePWS_QNAME, Boolean.class, Consultants.Consultant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "RenewConsultant", scope = Consultants.Consultant.class)
    public JAXBElement<Boolean> createConsultantsConsultantRenewConsultant(Boolean value) {
        return new JAXBElement<Boolean>(_ConsultantsConsultantRenewConsultant_QNAME, Boolean.class, Consultants.Consultant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "KitEnhancementEligible", scope = Consultants.Consultant.class)
    public JAXBElement<Boolean> createConsultantsConsultantKitEnhancementEligible(Boolean value) {
        return new JAXBElement<Boolean>(_ConsultantsConsultantKitEnhancementEligible_QNAME, Boolean.class, Consultants.Consultant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "DOB", scope = Consultants.Consultant.class)
    public JAXBElement<XMLGregorianCalendar> createConsultantsConsultantDOB(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ConsultantsConsultantDOB_QNAME, XMLGregorianCalendar.class, Consultants.Consultant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "LeadEligible", scope = Consultants.Consultant.class)
    public JAXBElement<Boolean> createConsultantsConsultantLeadEligible(Boolean value) {
        return new JAXBElement<Boolean>(_ConsultantsConsultantLeadEligible_QNAME, Boolean.class, Consultants.Consultant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "DirectorAgreementDate", scope = Consultants.Consultant.class)
    public JAXBElement<XMLGregorianCalendar> createConsultantsConsultantDirectorAgreementDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ConsultantsConsultantDirectorAgreementDate_QNAME, XMLGregorianCalendar.class, Consultants.Consultant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "DirectorConfirmationDate", scope = Consultants.Consultant.class)
    public JAXBElement<XMLGregorianCalendar> createConsultantsConsultantDirectorConfirmationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ConsultantsConsultantDirectorConfirmationDate_QNAME, XMLGregorianCalendar.class, Consultants.Consultant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "PWSExpirationDate", scope = Consultants.Consultant.class)
    public JAXBElement<XMLGregorianCalendar> createConsultantsConsultantPWSExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ConsultantsConsultantPWSExpirationDate_QNAME, XMLGregorianCalendar.class, Consultants.Consultant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "CancelPWS", scope = Consultants.Consultant.class)
    public JAXBElement<Boolean> createConsultantsConsultantCancelPWS(Boolean value) {
        return new JAXBElement<Boolean>(_ConsultantsConsultantCancelPWS_QNAME, Boolean.class, Consultants.Consultant.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pamperedchef.com/UpdateConsultant", name = "AutoRenewPWS", scope = Consultants.Consultant.class)
    public JAXBElement<Boolean> createConsultantsConsultantAutoRenewPWS(Boolean value) {
        return new JAXBElement<Boolean>(_ConsultantsConsultantAutoRenewPWS_QNAME, Boolean.class, Consultants.Consultant.class, value);
    }

}
