
package com.pamperedchef.getconsultantresponse;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.pamperedchef.getconsultantresponse package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.pamperedchef.getconsultantresponse
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Consultant }
     * 
     */
    public Consultant createConsultant() {
        return new Consultant();
    }

    /**
     * Create an instance of {@link Consultant.PhoneNumbers }
     * 
     */
    public Consultant.PhoneNumbers createConsultantPhoneNumbers() {
        return new Consultant.PhoneNumbers();
    }

    /**
     * Create an instance of {@link Consultant.Addresses }
     * 
     */
    public Consultant.Addresses createConsultantAddresses() {
        return new Consultant.Addresses();
    }

    /**
     * Create an instance of {@link AddressDetailType }
     * 
     */
    public AddressDetailType createAddressDetailType() {
        return new AddressDetailType();
    }

    /**
     * Create an instance of {@link Consultant.PhoneNumbers.PhoneNumber }
     * 
     */
    public Consultant.PhoneNumbers.PhoneNumber createConsultantPhoneNumbersPhoneNumber() {
        return new Consultant.PhoneNumbers.PhoneNumber();
    }

}
