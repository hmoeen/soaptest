
package com.pamperedchef.getconsultantresponse;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hybrisUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ConsultantID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Firstname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Lastname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Role">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Consultant"/>
 *               &lt;enumeration value="Consultant"/>
 *               &lt;enumeration value="Contact"/>
 *               &lt;enumeration value="Host"/>
 *               &lt;enumeration value="CoHost"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="eMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DOB" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="PhoneNumbers" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PhoneNumber" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="hybrisPhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="PhoneID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PhoneType">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="Home"/>
 *                                   &lt;enumeration value="Cell"/>
 *                                   &lt;enumeration value="Work"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="CountryDialingCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DefaultPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Addresses" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Address" type="{http://www.pamperedchef.com/GetConsultantResponse}AddressDetailType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DefaultShippingAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultBillingAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hybrisUserID",
    "consultantID",
    "firstname",
    "lastname",
    "code",
    "message",
    "role",
    "eMail",
    "dob",
    "phoneNumbers",
    "defaultPhone",
    "addresses",
    "defaultShippingAddressID",
    "defaultBillingAddressID"
})
@XmlRootElement(name = "Consultant")
public class Consultant {

    @XmlElement(required = true)
    protected String hybrisUserID;
    @XmlElement(name = "ConsultantID")
    protected String consultantID;
    @XmlElement(name = "Firstname", required = true)
    protected String firstname;
    @XmlElement(name = "Lastname", required = true)
    protected String lastname;
    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Message", required = true)
    protected String message;
    @XmlElement(name = "Role", required = true)
    protected String role;
    @XmlElement(required = true)
    protected String eMail;
    @XmlElement(name = "DOB")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dob;
    @XmlElement(name = "PhoneNumbers")
    protected Consultant.PhoneNumbers phoneNumbers;
    @XmlElement(name = "DefaultPhone")
    protected String defaultPhone;
    @XmlElement(name = "Addresses")
    protected Consultant.Addresses addresses;
    @XmlElement(name = "DefaultShippingAddressID")
    protected String defaultShippingAddressID;
    @XmlElement(name = "DefaultBillingAddressID")
    protected String defaultBillingAddressID;

    /**
     * Gets the value of the hybrisUserID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHybrisUserID() {
        return hybrisUserID;
    }

    /**
     * Sets the value of the hybrisUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHybrisUserID(String value) {
        this.hybrisUserID = value;
    }

    /**
     * Gets the value of the consultantID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsultantID() {
        return consultantID;
    }

    /**
     * Sets the value of the consultantID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsultantID(String value) {
        this.consultantID = value;
    }

    /**
     * Gets the value of the firstname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Sets the value of the firstname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstname(String value) {
        this.firstname = value;
    }

    /**
     * Gets the value of the lastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Sets the value of the lastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastname(String value) {
        this.lastname = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the eMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMail() {
        return eMail;
    }

    /**
     * Sets the value of the eMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMail(String value) {
        this.eMail = value;
    }

    /**
     * Gets the value of the dob property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDOB() {
        return dob;
    }

    /**
     * Sets the value of the dob property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDOB(XMLGregorianCalendar value) {
        this.dob = value;
    }

    /**
     * Gets the value of the phoneNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link Consultant.PhoneNumbers }
     *     
     */
    public Consultant.PhoneNumbers getPhoneNumbers() {
        return phoneNumbers;
    }

    /**
     * Sets the value of the phoneNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Consultant.PhoneNumbers }
     *     
     */
    public void setPhoneNumbers(Consultant.PhoneNumbers value) {
        this.phoneNumbers = value;
    }

    /**
     * Gets the value of the defaultPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultPhone() {
        return defaultPhone;
    }

    /**
     * Sets the value of the defaultPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultPhone(String value) {
        this.defaultPhone = value;
    }

    /**
     * Gets the value of the addresses property.
     * 
     * @return
     *     possible object is
     *     {@link Consultant.Addresses }
     *     
     */
    public Consultant.Addresses getAddresses() {
        return addresses;
    }

    /**
     * Sets the value of the addresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link Consultant.Addresses }
     *     
     */
    public void setAddresses(Consultant.Addresses value) {
        this.addresses = value;
    }

    /**
     * Gets the value of the defaultShippingAddressID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultShippingAddressID() {
        return defaultShippingAddressID;
    }

    /**
     * Sets the value of the defaultShippingAddressID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultShippingAddressID(String value) {
        this.defaultShippingAddressID = value;
    }

    /**
     * Gets the value of the defaultBillingAddressID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultBillingAddressID() {
        return defaultBillingAddressID;
    }

    /**
     * Sets the value of the defaultBillingAddressID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultBillingAddressID(String value) {
        this.defaultBillingAddressID = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Address" type="{http://www.pamperedchef.com/GetConsultantResponse}AddressDetailType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "address"
    })
    public static class Addresses {

        @XmlElement(name = "Address", required = true)
        protected List<AddressDetailType> address;

        /**
         * Gets the value of the address property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the address property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddress().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AddressDetailType }
         * 
         * 
         */
        public List<AddressDetailType> getAddress() {
            if (address == null) {
                address = new ArrayList<AddressDetailType>();
            }
            return this.address;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PhoneNumber" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="hybrisPhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="PhoneID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PhoneType">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="Home"/>
     *                         &lt;enumeration value="Cell"/>
     *                         &lt;enumeration value="Work"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="CountryDialingCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "phoneNumber"
    })
    public static class PhoneNumbers {

        @XmlElement(name = "PhoneNumber", required = true)
        protected List<Consultant.PhoneNumbers.PhoneNumber> phoneNumber;

        /**
         * Gets the value of the phoneNumber property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the phoneNumber property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPhoneNumber().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Consultant.PhoneNumbers.PhoneNumber }
         * 
         * 
         */
        public List<Consultant.PhoneNumbers.PhoneNumber> getPhoneNumber() {
            if (phoneNumber == null) {
                phoneNumber = new ArrayList<Consultant.PhoneNumbers.PhoneNumber>();
            }
            return this.phoneNumber;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="hybrisPhoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="PhoneID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PhoneType">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="Home"/>
         *               &lt;enumeration value="Cell"/>
         *               &lt;enumeration value="Work"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="CountryDialingCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsTextable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "hybrisPhoneID",
            "phoneID",
            "phoneType",
            "countryDialingCd",
            "number",
            "isTextable"
        })
        public static class PhoneNumber {

            protected String hybrisPhoneID;
            @XmlElement(name = "PhoneID", required = true)
            protected String phoneID;
            @XmlElement(name = "PhoneType", required = true)
            protected String phoneType;
            @XmlElement(name = "CountryDialingCd")
            protected String countryDialingCd;
            @XmlElement(name = "Number", required = true)
            protected String number;
            @XmlElement(name = "IsTextable")
            protected boolean isTextable;

            /**
             * Gets the value of the hybrisPhoneID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHybrisPhoneID() {
                return hybrisPhoneID;
            }

            /**
             * Sets the value of the hybrisPhoneID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHybrisPhoneID(String value) {
                this.hybrisPhoneID = value;
            }

            /**
             * Gets the value of the phoneID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneID() {
                return phoneID;
            }

            /**
             * Sets the value of the phoneID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneID(String value) {
                this.phoneID = value;
            }

            /**
             * Gets the value of the phoneType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneType() {
                return phoneType;
            }

            /**
             * Sets the value of the phoneType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneType(String value) {
                this.phoneType = value;
            }

            /**
             * Gets the value of the countryDialingCd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryDialingCd() {
                return countryDialingCd;
            }

            /**
             * Sets the value of the countryDialingCd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryDialingCd(String value) {
                this.countryDialingCd = value;
            }

            /**
             * Gets the value of the number property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumber() {
                return number;
            }

            /**
             * Sets the value of the number property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumber(String value) {
                this.number = value;
            }

            /**
             * Gets the value of the isTextable property.
             * 
             */
            public boolean isIsTextable() {
                return isTextable;
            }

            /**
             * Sets the value of the isTextable property.
             * 
             */
            public void setIsTextable(boolean value) {
                this.isTextable = value;
            }

        }

    }

}
