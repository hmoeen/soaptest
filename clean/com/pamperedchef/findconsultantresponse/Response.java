
package com.pamperedchef.findconsultantresponse;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Count" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Consultants" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Consultant" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ConsultantID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Firstname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Lastname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="eMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "count",
    "code",
    "message",
    "consultants"
})
@XmlRootElement(name = "Response")
public class Response {

    @XmlElement(name = "Count", required = true)
    protected BigInteger count;
    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Message", required = true)
    protected String message;
    @XmlElement(name = "Consultants")
    protected Response.Consultants consultants;

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCount(BigInteger value) {
        this.count = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the consultants property.
     * 
     * @return
     *     possible object is
     *     {@link Response.Consultants }
     *     
     */
    public Response.Consultants getConsultants() {
        return consultants;
    }

    /**
     * Sets the value of the consultants property.
     * 
     * @param value
     *     allowed object is
     *     {@link Response.Consultants }
     *     
     */
    public void setConsultants(Response.Consultants value) {
        this.consultants = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Consultant" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ConsultantID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Firstname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Lastname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="eMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "consultant"
    })
    public static class Consultants {

        @XmlElement(name = "Consultant", required = true)
        protected List<Response.Consultants.Consultant> consultant;

        /**
         * Gets the value of the consultant property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the consultant property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getConsultant().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Response.Consultants.Consultant }
         * 
         * 
         */
        public List<Response.Consultants.Consultant> getConsultant() {
            if (consultant == null) {
                consultant = new ArrayList<Response.Consultants.Consultant>();
            }
            return this.consultant;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ConsultantID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ConsultantNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Firstname" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Lastname" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="eMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "consultantID",
            "consultantNumber",
            "firstname",
            "lastname",
            "eMail",
            "city",
            "state",
            "postalCode"
        })
        public static class Consultant {

            @XmlElement(name = "ConsultantID", required = true)
            protected String consultantID;
            @XmlElement(name = "ConsultantNumber", required = true)
            protected String consultantNumber;
            @XmlElement(name = "Firstname", required = true)
            protected String firstname;
            @XmlElement(name = "Lastname", required = true)
            protected String lastname;
            @XmlElement(required = true)
            protected String eMail;
            @XmlElement(name = "City")
            protected String city;
            @XmlElement(name = "State")
            protected String state;
            @XmlElement(name = "PostalCode")
            protected String postalCode;

            /**
             * Gets the value of the consultantID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getConsultantID() {
                return consultantID;
            }

            /**
             * Sets the value of the consultantID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setConsultantID(String value) {
                this.consultantID = value;
            }

            /**
             * Gets the value of the consultantNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getConsultantNumber() {
                return consultantNumber;
            }

            /**
             * Sets the value of the consultantNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setConsultantNumber(String value) {
                this.consultantNumber = value;
            }

            /**
             * Gets the value of the firstname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstname() {
                return firstname;
            }

            /**
             * Sets the value of the firstname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstname(String value) {
                this.firstname = value;
            }

            /**
             * Gets the value of the lastname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastname() {
                return lastname;
            }

            /**
             * Sets the value of the lastname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastname(String value) {
                this.lastname = value;
            }

            /**
             * Gets the value of the eMail property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEMail() {
                return eMail;
            }

            /**
             * Sets the value of the eMail property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEMail(String value) {
                this.eMail = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the state property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getState() {
                return state;
            }

            /**
             * Sets the value of the state property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setState(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the postalCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostalCode() {
                return postalCode;
            }

            /**
             * Sets the value of the postalCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostalCode(String value) {
                this.postalCode = value;
            }

        }

    }

}
