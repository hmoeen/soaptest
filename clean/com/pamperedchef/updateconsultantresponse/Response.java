
package com.pamperedchef.updateconsultantresponse;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Consultant" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ConsultantID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Addresses" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="hybrisAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultant"
})
@XmlRootElement(name = "Response")
public class Response {

    @XmlElement(name = "Consultant", required = true)
    protected List<Response.Consultant> consultant;

    /**
     * Gets the value of the consultant property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consultant property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsultant().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Response.Consultant }
     * 
     * 
     */
    public List<Response.Consultant> getConsultant() {
        if (consultant == null) {
            consultant = new ArrayList<Response.Consultant>();
        }
        return this.consultant;
    }


    /**
     * Unique ConsultantID (CDH)
     * 
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ConsultantID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Addresses" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="hybrisAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "consultantID",
        "addresses",
        "code",
        "message"
    })
    public static class Consultant {

        @XmlElement(name = "ConsultantID", required = true)
        protected String consultantID;
        @XmlElement(name = "Addresses")
        protected Response.Consultant.Addresses addresses;
        @XmlElement(name = "Code", required = true)
        protected String code;
        @XmlElement(name = "Message", required = true)
        protected String message;

        /**
         * Gets the value of the consultantID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConsultantID() {
            return consultantID;
        }

        /**
         * Sets the value of the consultantID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConsultantID(String value) {
            this.consultantID = value;
        }

        /**
         * Gets the value of the addresses property.
         * 
         * @return
         *     possible object is
         *     {@link Response.Consultant.Addresses }
         *     
         */
        public Response.Consultant.Addresses getAddresses() {
            return addresses;
        }

        /**
         * Sets the value of the addresses property.
         * 
         * @param value
         *     allowed object is
         *     {@link Response.Consultant.Addresses }
         *     
         */
        public void setAddresses(Response.Consultant.Addresses value) {
            this.addresses = value;
        }

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the message property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMessage() {
            return message;
        }

        /**
         * Sets the value of the message property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMessage(String value) {
            this.message = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="hybrisAddressID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AddressID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "hybrisAddressID",
            "addressID",
            "code",
            "message"
        })
        public static class Addresses {

            protected String hybrisAddressID;
            @XmlElement(name = "AddressID", required = true)
            protected String addressID;
            @XmlElement(name = "Code", required = true)
            protected String code;
            @XmlElement(name = "Message", required = true)
            protected String message;

            /**
             * Gets the value of the hybrisAddressID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHybrisAddressID() {
                return hybrisAddressID;
            }

            /**
             * Sets the value of the hybrisAddressID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHybrisAddressID(String value) {
                this.hybrisAddressID = value;
            }

            /**
             * Gets the value of the addressID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddressID() {
                return addressID;
            }

            /**
             * Sets the value of the addressID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddressID(String value) {
                this.addressID = value;
            }

            /**
             * Gets the value of the code property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Sets the value of the code property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Gets the value of the message property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMessage() {
                return message;
            }

            /**
             * Sets the value of the message property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMessage(String value) {
                this.message = value;
            }

        }

    }

}
